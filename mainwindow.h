#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void update6dof( int len1,int len2,int len3,int len4);


    float len1_6dof=100;
    float len2_6dof=100;
    float len3_6dof=100;
    float len4_6dof=100;

    float len1_delta=100;
    float len2_delta=100;
    float len3_delta=100;
    float len4_delta=100;

    float len1_4dof=100;
    float len2_4dof=100;

    float len1_scara=100;
    float len2_scara=100;

    float xEnd;
    float yEnd;
    float zEnd;

    void setXEnd(float val);
    void setYEnd(float val);
    void setZEnd(float val);

    void change6dof(float x,float y,float z);

private slots:
    void on_slider_aBase_2_valueChanged(int value);

    void on_slider_a1_2_valueChanged(int value);

    void on_slider_a2_2_valueChanged(int value);

    void on_slider_a3_2_valueChanged(int value);

    void on_slider_medR_2_valueChanged(int value);

    void on_slider_lowR_2_valueChanged(int value);

    void on_slider_grip_2_valueChanged(int value);

    void on_slider_viewX_2_valueChanged(int value);

    void on_slider_viewY_2_valueChanged(int value);

    void on_slider_viewZ_2_valueChanged(int value);

    void on_camRotX_valueChanged(int value);

    void on_camRotY_valueChanged(int value);

    void on_camRotZ_valueChanged(int value);

    void on_spinBox_len1_6dof_valueChanged(int arg1);

    void on_spinBox_len2_6dfof_valueChanged(int arg1);

    void on_spinBox_len3_6dof_valueChanged(int arg1);

    void on_spinBox_len1_4dof_valueChanged(int arg1);

    void on_spinBox_len2_4dof_valueChanged(int arg1);

    void on_spinBox_len1_scara_valueChanged(int arg1);

    void on_spinBox_len2_scara_valueChanged(int arg1);

    void on_spinBox_lenBaseTop_delta_valueChanged(int arg1);

    void on_spinBox_lenBaseBottom_delta_valueChanged(int arg1);

    void on_spinBox_lenJointTop_delta_valueChanged(int arg1);

    void on_spinBox_lenJointBottom_Delta_valueChanged(int arg1);

    void on_spinBox_len4_6dof_valueChanged(int arg1);

    void on_slider_x_2_sliderMoved(int position);

    void on_slider_y_2_sliderMoved(int position);

    void on_slider_z_2_sliderMoved(int position);

private:
    Ui::MainWindow *ui;
    QSerialPort *serial;
};

#endif // MAINWINDOW_H
