#ifndef OWIDGET_H
#define OWIDGET_H

#include <QWidget>
#include <QOpenGLWidget>
#include <gl/GLU.h>
#include <gl/GL.h>
#include <QTimer>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>

class owidget: public QOpenGLWidget

{
public:
    owidget(QWidget *parent = 0);
    ~owidget();

    void scene();

    void joint(float jointDiam,float jointLen,float xBegin,float YBegin,float zBegin,float xRotate,float yRotate,float zRotate);

    void draw6dof(float len1, float len2, float len3, float angleBase, float angle1, float angle2, float angle3, float angle4, float len4,float medR,float lowR);

    void drawDelta(float len1,float len2,float len3,float len4,float angle1,float angle2,float angle3);

    void draw4dof(float len1,float len2,float angle1,float angle2,float angleBase);

    void drawScara(float len1,float len2,float yPos,float angle1,float angle2);

    void cube(float size);
    void coordVisual(float size);
    void drawRobot(float len1, float len2, float len3, float len4, float angle1, float angle2, float angle3, float angleMedR, float angleLowR, float angleGrip, float angleBase, int robotType, bool inverse);

    float xEnd;
    float yEnd;
    float zEnd;

    float a1=45;
    float a2=45;
    float a3=45;
    float aBase=45;
    float medR;
    float lowR;
    float grip=45;
    float camX;
    float camY=-62;
    float camZ=-555;
    float camRotX=22;
    float camRotY=-51;
    float camRotZ;
    int robotType=1;
    bool inverse;
    float len1=100;
    float len2=100;
    float len3=100;
    float len4=20;

    void changeA1(float val);
    void changeA2(float val);
    void changeA3(float val);
    void changeABase(float val);
    void changeMedR(float val);
    void changeLowR(float val);
    void changeGrip(float val);
    void changeCamX(int val);
    void changeCamY(int val);
    void changeCamZ(int val);
    void changeXEnd(int val);
    void changeYEnd(int val);
    void changeZEnd(int val);
    void changeCamRotX(int value);
    void changeCamRotY(int value);
    void changeCamRotZ(int value);
    void updateRobot(int value, float len1t, float len2t, float len3t, float len4t);
    void changeInverse(bool inverseVal);
    float *getValues();

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
};

#endif // OWIDGET_H
