#include "owidget.h"
#include <QtCore>
#include <QOpenGLWidget>
#include <GL/glu.h>
#include <GL/gl.h>
#include <compute.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"

owidget::owidget(QWidget *parent)
    : QOpenGLWidget(parent)
{

}

owidget::~owidget()
{

}
void owidget::coordVisual(float size)
{
    glColor3f(1,0,0);
    glBegin(GL_LINES);
        glVertex3f(0,0,0);
        glVertex3f(0,size,0);
    glEnd();
    glColor3f(0,1,0);
    glBegin(GL_LINES);
        glVertex3f(0,0,0);
        glVertex3f(size,0,0);
    glEnd();
    glColor3f(0,0,1);
    glBegin(GL_LINES);
        glVertex3f(0,0,0);
        glVertex3f(0,0,size);
    glEnd();
}


void owidget::joint(float jointRad,float jointLen,float xBegin,float yBegin,float zBegin,float xRotate,float yRotate,float zRotate)
{
    glPushMatrix();

        glTranslatef(xBegin,yBegin,zBegin);
        glRotatef(xRotate,1,0,0);
        glRotatef(yRotate,0,1,0);
        glRotatef(zRotate,0,0,1);
        coordVisual(30);
        glColor3f(0,1,0);
        glBegin(GL_LINE_LOOP);
            glVertex3f(0,-jointRad,-jointRad);
            glVertex3f(0,-jointRad,jointRad);
            glVertex3f(0,jointRad,jointRad);
            glVertex3f(0,jointRad,-jointRad);
        glEnd();
        glBegin(GL_LINES);
            glVertex3f(0,-jointRad,-jointRad);
            glVertex3f(jointLen,-jointRad,-jointRad);
            glVertex3f(0,-jointRad,jointRad);
            glVertex3f(jointLen,-jointRad,jointRad);
            glVertex3f(0,jointRad,jointRad);
            glVertex3f(jointLen,jointRad,jointRad);
            glVertex3f(0,jointRad,-jointRad);
            glVertex3f(jointLen,jointRad,-jointRad);
        glEnd();
        glBegin(GL_LINE_LOOP);
            glVertex3f(jointLen,-jointRad,-jointRad);
            glVertex3f(jointLen,-jointRad,jointRad);
            glVertex3f(jointLen,jointRad,jointRad);
            glVertex3f(jointLen,jointRad,-jointRad);
        glEnd();




    glPopMatrix();
}

void owidget::scene()
{
    glColor3f(1,1,1);
    glBegin(GL_LINES);
        glVertex3f(-200,0,200);
        glVertex3f(200,0,200);
        glVertex3f(200,0,-200);
        glVertex3f(200,0,200);
        glVertex3f(-200,0,-200);
        glVertex3f(-200,0,200);
        glVertex3f(-200,0,-200);
        glVertex3f(200,0,-200);
    glEnd();
}

void owidget::draw6dof(float len1,float len2,float len3,float angleBase,float angle1,float angle2,float angle3,float angle4,float len4,float medR,float lowR)
{
        joint(5,len1,0,0,0,0,angleBase,angle1);

        float *result;

        result = forward6dof(len1,len2,len3,angle1,angle2,angle3,angleBase);

        if (!true) {
            joint(5,len2, result[4], result[5], -result[6] ,/*medR*/0 ,angleBase, -90 + angle1 + angle2);
        } else {
            joint(5,len2, result[4], result[5], -result[6] ,/*medR*/0,angleBase , -90 + angle1 + angle2);
        }


        if (!true) {
            joint(5,len3,result[7],result[8],-result[9],/*lowR+medR*/0,angleBase, -180 + angle1 + angle2 + angle3);
        } else {
            joint(5,len3,result[7],result[8],-result[9], /*lowR+medR*/0,angleBase,180 + angle1 + angle2 + angle3);
        }
        xEnd=result[1];
        yEnd=result[2];
        zEnd=result[3];

        joint(5,len4,result[1],result[2],-result[3],0,angleBase,-270 + angle1 + angle2 + angle3 +angle4);


}


void owidget::drawDelta(float len1,float len2,float len3,float len4,float angle1,float angle2,float angle3)
{

}

void owidget::draw4dof(float len1,float len2,float angle1,float angle2,float angleBase)
{

}

void owidget::drawScara(float len1,float len2,float yPos,float angle1,float angle2)
{

}

 void owidget::drawRobot(float len1, float len2, float len3, float len4, float angle1, float angle2, float angle3, float angleMedR, float angleLowR, float angleGrip,float angleBase, int robotType,bool inverse)
{

     switch(robotType)
     {
        case 1:
            draw6dof(len1,len2,len3,angleBase,angle1,angle2,angle3,angleGrip,len4,angleMedR,angleLowR);
            break;
        case 2:
            draw4dof(len1,len2,angle1,angle2,angleBase);
            break;
        case 3:
            drawDelta(len1,len1,len3,len4,angle1,angle2,angle3);
            break;
        case 4:
            drawScara(len1,len2,angleBase,angle1,angle2);
            break;
     }
}




void owidget::initializeGL()
{
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-0.5,0.5,-0.5,0.5,1,1200);
    glMatrixMode(GL_MODELVIEW);
}

void owidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
        glTranslatef(camX,-10+camY,-50+camZ);
        glRotatef(camRotX,1,0,0);
        glRotatef(camRotY,0,1,0);
        glRotatef(camRotZ,0,0,1);
        scene();
        coordVisual(200);
        drawRobot(len1,len2,len3,len4,a1,a2,a3,medR,lowR,grip,aBase,robotType,inverse);
    glPopMatrix();
}







































void owidget::resizeGL(int width, int height)
{
    glViewport(0,0,width,height);
}
void owidget::changeA1(float val)
{
    a1=val;
    owidget::update();
}
void owidget::changeA2(float val)
{
    a2=val;
    owidget::update();
}
void owidget::changeA3(float val)
{
    a3=val;
    owidget::update();
}
void owidget::changeABase(float val)
{
    aBase=val;
    owidget::update();
}
void owidget::changeMedR(float val)
{
    medR=val;
    owidget::update();
}
void owidget::changeLowR(float val)
{
    lowR=val;
    owidget::update();
}
void owidget::changeGrip(float val)
{
    grip=val;
    owidget::update();
}
void owidget::changeCamX(int val)
{
    camX=val;
    owidget::update();
}
void owidget::changeCamY(int val)
{
    camY=val;
    owidget::update();
}
void owidget::changeCamZ(int val)
{
    camZ=val;
    owidget::update();
}
void owidget::changeXEnd(int val)
{
    xEnd=val;
    owidget::update();
}
void owidget::changeYEnd(int val)
{
    yEnd=val;
    owidget::update();
}
void owidget::changeZEnd(int val)
{
    zEnd=val;
    owidget::update();
}
void owidget::changeCamRotX(int value)
{
    camRotX=value;
    owidget::update();
}
void owidget::changeCamRotY(int value)
{
    camRotY=value;
    owidget::update();
}
void owidget::changeCamRotZ(int value)
{
    camRotZ=value;
    owidget::update();
}
void owidget::updateRobot(int value, float len1t, float len2t, float len3t, float len4t)
{
    robotType=value;
    len1= len1t;
    len2= len2t;
    len3= len3t;
    len4= len4t;
    owidget::update();
}
void owidget::changeInverse(bool inverseVal)
{
    inverse=inverseVal;
    owidget::update();
}
float *owidget::getValues()
{
    static float returnResult[3];
    returnResult[0]=xEnd;
    returnResult[1]=yEnd;
    returnResult[2]=zEnd;
    return returnResult;

}
