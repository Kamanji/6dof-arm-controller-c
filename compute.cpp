#include <math.h>

double degrees(double radians)
{
   return (radians * 180.0) / ((double) M_PI);
}

double radians(double degrees){
    return ((degrees*M_PI)/180 );
}

float delta_calcAngleYZ(float x0, float y0, float z0,float len1 , float len2, float len3,float len4)
{
    float y1 = -0.5 * 0.57735 * len1;
    float resAngle;
    y0 -= 0.5 * 0.57735 * len4;

    float a = (x0*x0 + y0*y0 + z0*z0 +len2*len2 - len3*len3 - y1*y1)/(2*z0);
    float b = (y1-y0)/z0;

    float d = -(a+b*y1)*(a+b*y1)+len2*(b*b*len2+len2);
    if (d < 0) return -1;
    float yj = (y1 - a*b - sqrt(d))/(b*b + 1);
    float zj = a + b*yj;
    resAngle = degrees(atan(-zj/(y1 - yj)) + ((yj>y1)?180.0:0.0));
    return resAngle;
}

float baseAngle(float x, float z)
{
    float hip = sqrt(x*x+z*z);
    float resAngle;

    if(x>0&&z>0){
        resAngle =  degrees(acos(hip/x));
    }else if(x>0&&z<0){
        resAngle =180-degrees(acos(hip/x));
    }else if(x<0&&z<0){
        resAngle =180+degrees(acos(hip/x));
    }else{
        resAngle =360-degrees(acos(hip/x));
    }
    return resAngle;
}

float baseCoords(float len, float angle,char mod)
{
    float x,z;
    if(angle<=90){
        x = len*cos(radians(angle));
        z = len*sin(radians(angle));
    }else if(angle>90&&angle<=180){
        x = -len*cos(radians(180-angle));
        z = len*sin(radians(180-angle));
    }else if(angle>180&&angle<=270){
        x = -len*cos(radians(angle-180));
        z = -len*sin(radians(angle-180));
    }else{
        x = len*cos(radians(360-angle));
        z = -len*sin(radians(360-angle));
    }
    if(mod=='x'){
        return x;
    }else{
        return z;
    }
}

float * forward6dof(float len1,float len2,float len3,float a1,float a2,float a3,float aBase)
{

    static float res_6dof_forward[9];
    /*
     * [1] - X coord of the end affector
     * [2] - Y coord of the end affector
     * [3] - Z coord of the end affector
     * [4] - X coord of the second joint beginning
     * [5] - Y coord of the second joint beginning
     * [6] - Z coord of the second joint beginning
     * [7] - X coord of the third joint beginning
     * [8] - Y coord of the third joint beginning
     * [9] - Z coord of the third joint beginning
     */



    // Calculating position of second joint start position
    float yb = sin(radians(a1)) * len1;
    float AT = sqrt(len1 * len1 - yb * yb);
    float zb = sin(radians(aBase)) * AT;
    float xb = cos(radians(aBase)) * AT;

    zb=(a1>90)?-zb:zb;
    xb=(a1>90)?-xb:xb;



    res_6dof_forward[4]=xb;
    res_6dof_forward[5]=yb;
    res_6dof_forward[6]=zb;

    //Calculating position of third joint start position

    float AC = sqrt(len1 * len1 + len2 * len2 - 2 * len1 * len2 * cos(radians(90 + a2)));
    float BAC = degrees(acos((len1 * len1 + AC * AC - len2 * len2) / (2 * len1 * AC)));
    float CAF = ((90 + a2) > 180) ? a1 + BAC : a1 - BAC;

    float yc = sin(radians(CAF)) * AC;
    float AF = sqrt(AC * AC - yc * yc);

    float zc = sin(radians(aBase)) * AF;
    float xc = cos(radians(aBase)) * AF;

    xc = (CAF > 90) ? -1 * xc : xc;
    zc = (CAF > 90) ? -1 * zc : zc;

    res_6dof_forward[7]=xc;
    res_6dof_forward[8]=yc;
    res_6dof_forward[9]=zc;

    // Fouth joint

    float BAD, AD, DAG;

    if ((a2 + 90) <= 180) {

        if ((90 + a3) <= 180) {

            float BD = sqrt(len2 * len2 + len3 * len3 - 2 * len2 * len3 * cos(radians(90 + a3)));
            float CBD = degrees(acos((len2 * len2 + BD * BD - len3 * len3) / (2 * len2 * BD)));
            float ABD = (90 + a2) - CBD;
            AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
            BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
            DAG = a1 - BAD;

        } else {

            float BD = sqrt(len2 * len2 + len3 * len3 - 2 * len2 * len3 * cos(radians(270 - a3)));
            float DBC = degrees(acos((BD * BD + len2 * len2 - len3 * len3) / (2 * BD * len2)));
            float ABD = 90 + a2 + DBC;

            if (ABD >= 180) {
                AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(360 - ABD)));
                BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                DAG = a1 + BAD;
            } else {
                AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
                BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                DAG = a1 - BAD;
            }

        }
    } else {
        if ((90 + a3) <= 180) {

            float BD = sqrt(len2 * len2 + len3 * len3 - 2 * len2 * len3 * cos(radians(90 + a3)));
            float CBD = degrees(acos((len2 * len2 + BD * BD - len3 * len3) / (2 * len2 * BD)));
            float ABD = (90 + a2) - CBD;

            if(ABD>=180){
                AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
                BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                DAG = a1 + BAD;
            }else{
                AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
                BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                DAG = a1 - BAD;
            }



        } else {

            float BD = sqrt(len2 * len2 + len3 * len3 - 2 * len2 * len3 * cos(radians(270 - a3)));
            float DBC = degrees(acos((BD * BD + len2 * len2 - len3 * len3) / (2 * BD * len2)));
            float ABD = 90 + a2 + DBC;
            AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
            BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
            DAG = a1 + BAD;

        }

    }

    float yd = sin(radians(DAG)) * AD;
    float AG = sqrt(AD * AD - yd * yd);

    float zd = sin(radians(aBase)) * AG;
    float xd = cos(radians(aBase)) * AG;

    xd = (DAG > 90) ? -1 * xd : xd;
    zd = (DAG > 90) ? -1 * zd : zd;

    res_6dof_forward[1]=xd;
    res_6dof_forward[2]=yd;
    res_6dof_forward[3]=zd;

    return res_6dof_forward;

}

float * forwardDelta(float a1, float a2, float a3,float len1,float len2,float len3,float len4)
{
    static float res_delta_forward[18];

    float t = (len1-len4)*tan(radians(30.0))/2;

    float y1 = -(t + len2*cos(radians(a1)));
    float z1 = -len2*sin(radians(a1));

    float y2 = (t + len2*cos(radians(a2)))*sin(radians(30));
    float x2 = y2*tan(radians(60));
    float z2 = -len2*sin(radians(a2));

    float y3 = (t + len2*cos(radians(a3)))*sin(radians(30));
    float x3 = -y3*tan(radians(60));
    float z3 = -len2*sin(radians(a3));

    float dnm = (y2-y1)*x3-(y3-y1)*x2;

    float w1 = y1*y1 + z1*z1;
    float w2 = x2*x2 + y2*y2 + z2*z2;
    float w3 = x3*x3 + y3*y3 + z3*z3;

    // x = (a1*z + b1)/dnm
    float A1 = (z2-z1)*(y3-y1)-(z3-z1)*(y2-y1);
    float b1 = -((w2-w1)*(y3-y1)-(w3-w1)*(y2-y1))/2.0;

    // y = (a2*z + b2)/dnm;
    float A2 = -(z2-z1)*x3+(z3-z1)*x2;
    float b2 = ((w2-w1)*x3 - (w3-w1)*x2)/2.0;

    // a*z^2 + b*z + c = 0
    float a = A1*A1 + A2*A2 + dnm*dnm;
    float b = 2*(A1*b1 + A2*(b2-y1*dnm) - z1*dnm*dnm);
    float c = (b2-y1*dnm)*(b2-y1*dnm) + b1*b1 + dnm*dnm*(z1*z1 - len3*len3);

    float d = b*b - (float)4.0*a*c;
    if (d < 0){
        res_delta_forward[1]=-1;
    } else{
        res_delta_forward[3] = -(float)0.5*(b+sqrt(d))/a;
        res_delta_forward[1] = (A1*res_delta_forward[3] + b1)/dnm;
        res_delta_forward[2] = (A2*res_delta_forward[3] + b2)/dnm;
    }
        return res_delta_forward;

}

float * inverseDelta(float x0, float y0, float z0,float len1 , float len2, float len3,float len4)
{
    static float res_delta_inverse[3];
    float cos120 = cos(radians(120));
    float sin120 = sin(radians(120));
    float a1 = delta_calcAngleYZ(x0, y0, z0,len1 ,len2,  len3, len4 );

    if(a1==-1){
        res_delta_inverse[1]=-1;
        return res_delta_inverse;
    }else{
        res_delta_inverse[1]=a1;
    }
    float a2 = delta_calcAngleYZ(x0*cos120 + y0*sin120, y0*cos120-x0*sin120, z0,len1 ,len2,  len3, len4 );

    if(a2==-1){
        res_delta_inverse[1]=-1;
        return res_delta_inverse;
    }else{
        res_delta_inverse[2]=a2;
    }
    float a3 = delta_calcAngleYZ(x0*cos120 - y0*sin120, y0*cos120+x0*sin120, z0,len1 ,len2,  len3, len4 );

    if(a3==-1){
        res_delta_inverse[1]=-1;
        return res_delta_inverse;
    }else{
        res_delta_inverse[3]=a3;
    }

    return res_delta_inverse;
}

float * inverse6dof(float len1,float len2,float len3,float x,float y,float z)
{

    static float res_6dof_inverse[4];

    /*
     * [1] - A1 angle
     * [2] - A2 angle
     * [3] - A3 angle
     * [4] - base angle
     */

    float AG = sqrt(x*x + z*z);
    float AD = sqrt(AG * AG + y*y);
    float DAG = degrees(asin(y/AD));

    float aBase = baseAngle(x,-z);


    for (int a2 = 90; a2 <= 180; a2++) {
        for (int a3 = 90; a3 <= 180; a3++) {

            float BD = sqrt(len2 * len2 + len3 * len3 - 2 * len2 * len3 * cos(radians(a3)));
            float CBD = degrees(acos((len2 * len2 + BD * BD - len3 * len3) / (2 * len2 * BD)));
            float ADt = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(a2 - CBD)));
            float BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));

            if ((AD + 1 > ADt) && (AD - 1 < ADt)) {
                res_6dof_inverse[1]=(float)(DAG+BAD);
                res_6dof_inverse[2]=(float)(a2);
                res_6dof_inverse[3]=(float)(a3);
                res_6dof_inverse[4]=(float)(aBase);
            }
        }
    }


    return res_6dof_inverse;
}

float * inverseScara(float len1,float len2,float x,float y, float z)
{
    static float resScaraInverse[3];
    /*
     * [1] - Y height
     * [2] - A1 angle
     * [3] - A2 angle
     */
    resScaraInverse[1] =y;
    float AC = sqrt(x*x+z*z);

    float ABC = degrees(acos((len1*len1+len2*len2-AC*AC)/(2*len1*len2)));

    resScaraInverse[2] =ABC;
    if(x>0&&z>0){
        resScaraInverse[3] =degrees(acos(AC/x));
    }else if(x>0&&z<0){
        resScaraInverse[3] =180-degrees(acos(AC/x));
    }else if(x<0&&z<0){
        resScaraInverse[3] =180+degrees(acos(AC/x));
    }else{
        resScaraInverse[3] =360-degrees(acos(AC/x));
    }
    return resScaraInverse;

}

float * forwardScara(float len1,float len2,float a1,float a2,float yPos)
{
    static float resScaraForward[6];

    resScaraForward[2]=yPos;
    resScaraForward[5]=yPos;

    float AC = sqrt(len1*len1+len2*len2-2*len1*len2*cos(radians(a2)));
    float BAC = degrees(acos((len1*len1+AC*AC-len2*len2)/(2*len1*AC)));
    float angle = a1 - BAC;

    resScaraForward[1] = baseCoords(AC,angle,'x');
    resScaraForward[3] = baseCoords(AC,angle,'z');
    resScaraForward[4] = baseCoords(len1,angle,'x');
    resScaraForward[6] = baseCoords(len1,angle,'x');



    return resScaraForward;
}

float * forward4dof(float len1,float len2,float aBase,float a1,float a2)
{
    static float res_4dof_forward[6];
    // [1-3] xyz end affector [4-6] xyz middle joint begin
    float AC = sqrt(len1*len1+len2*len2-2*len1*len2*cos(a2));
    float BAX =degrees(acos((len1*len1+AC*AC-len2*len2)/(2*len1*AC)));
    float CAX = a1-BAX;
    res_4dof_forward[2]=AC*sin(radians(CAX));
    float AX =AC*cos(radians(CAX));

    res_4dof_forward[1] = baseCoords(AX,aBase,'x');
    res_4dof_forward[3] = baseCoords(AX,aBase,'z');

    res_4dof_forward[5]=len1*sin(radians(a1));
    AX = len1*cos(radians(a1));

    res_4dof_forward[4] = baseCoords(AX,aBase,'x');
    res_4dof_forward[6] = baseCoords(AX,aBase,'x');

    return res_4dof_forward;
}

float * inverse4dof(float len1,float len2,float x,float y,float z)
{
    static float res_4dof_inverse[3];

    float AD = sqrt(x*x+y*y);
    float AC = sqrt(AD*AD+y*y);

    float BAC = degrees(acos((len1*len1+AC*AC-len2*len2)/(2*len1*AC)));
    float ABC = degrees(acos((len1*len1+len2*len2-AC*AC)/(2*len1*len2)));
    float CAD = degrees(acos((AD*AD+AC*AC-y*y)/(2*AD*AC)));
    float BAD = CAD + BAC;

    float aBase = baseAngle(x,z);

    res_4dof_inverse[1] = aBase;
    res_4dof_inverse[2] = BAD;
    res_4dof_inverse[3] = ABC;


    return res_4dof_inverse;
}
